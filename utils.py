"""Utility classes for Snake Game"""

from typing import NamedTuple
from enum import Enum


class Direction(Enum):
    Up = "Up"
    Left = "Left"
    Down = "Down"
    Right = "Right"


class Pos(NamedTuple('Point', [('x', int), ('y', int)])):
    @staticmethod
    def from_dir(direction: Direction):
        """Get position Vector from Direction"""
        if direction == Direction.Up:
            return Pos(0, -1)
        elif direction == Direction.Left:
            return Pos(-1, 0)
        elif direction == Direction.Down:
            return Pos(0, 1)
        elif direction == Direction.Right:
            return Pos(1, 0)
        else:
            raise ValueError(f"Invalid value {direction} for Direction")

    def __str__(self):
        return f"({self.x}, {self.y})"

    def __add__(self, other):
        if isinstance(other, Pos):
            return Pos(self.x + other.x, self.y + other.y)
        raise TypeError(f"Cannot add type '{type(other).__name__}' "
                        "to type 'Pos'")

    def __sub__(self, other):
        if isinstance(other, Pos):
            return Pos(self.x - other.x, self.y - other.y)
        raise TypeError(f"Cannot subtract type '{type(other).__name__}' "
                        "from type 'Pos'")

    def __mul__(self, other: int):
        return Pos(self.x * other, self.y * other)

    def __neg__(self):
        return Pos(-self.x, -self.y)
