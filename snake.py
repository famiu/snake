"""Implementation of Snake game backend"""

import typing
import utils


class Snake:
    """Class that represents a 'Snake', which is the entity being controlled"""

    def __init__(self, direction=utils.Direction.Right, size=3,
                 head_pos=utils.Pos(16, 16)):
        if size < 2:
            raise ValueError("Snake size cannot be lesser than two")

        self.direction = direction
        self.head_pos = head_pos
        self.body_pos: typing.List[utils.Pos] = []
        self.score = 0

        # Since head already gives the body a size of 1,
        # start at 1 and then add body parts in the opposite direction.
        # The opposite direction is found by getting the snake's direction
        # and negating it / subtracting it from head_pos
        for i in range(1, size):
            self.body_pos.append(self.head_pos -
                                 utils.Pos.from_dir(self.direction) * i)

    def self_collision(self) -> bool:
        """Check if snake's body collides with itself"""

        for body_part_pos in self.body_pos:
            if body_part_pos == self.head_pos:
                return True
        return False

    def move(self) -> None:
        """Move the snake"""

        # Move the head according to direction. Then, starting with head,
        # use a temporary variable to store the previous position of the
        # last moved body_part, then set the value of the body part after it
        # to that variable and assign a new value to temp
        self.body_pos.pop()
        self.body_pos.insert(0, self.head_pos)
        self.head_pos += utils.Pos.from_dir(self.direction)

    def grow(self) -> None:
        """Grow the snake's size by one"""

        # Get the difference between the last and second last body part
        # positions, and add that to the last body part position to
        # get the new body part position
        self.body_pos.append(self.body_pos[-1] +
                             self.body_pos[-1] - self.body_pos[-2])
