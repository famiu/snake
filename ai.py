from player import Player
from game import Game
from utils import Direction


class AI(Player):
    """AI controlling the Snake"""

    def __init__(self):
        super().__init__(True)

    def get_input(self):
        matching_direction = {
            Direction.Up: Direction.Left,
            Direction.Left: Direction.Down,
            Direction.Down: Direction.Right,
            Direction.Right: Direction.Up
        }[self.game.snake.direction]

        self.game.set_snake_direction(matching_direction)
