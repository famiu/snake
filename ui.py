import threading
import time
import pygame
from game import Game
from utils import Direction
from player import Player


class PyGameHuman(Player):
    """Human player for PyGameUI."""

    def __init__(self):
        super().__init__(False)

    def input_loop(self):
        while not self.game_ended:
            for event in pygame.event.get():
                if(event.type == pygame.KEYDOWN and
                     event.key == pygame.K_RETURN):
                    self.game.paused = not self.game.paused
                elif event.type == pygame.KEYDOWN and not self.game.paused:
                    if event.key == pygame.K_UP:
                        self.game.set_snake_direction(Direction.Up)
                    elif event.key == pygame.K_LEFT:
                        self.game.set_snake_direction(Direction.Left)
                    elif event.key == pygame.K_DOWN:
                        self.game.set_snake_direction(Direction.Down)
                    elif event.key == pygame.K_RIGHT:
                        self.game.set_snake_direction(Direction.Right)


class PyGameUI:
    """PyGame frontend for the Snake game."""

    def __init__(self, player: Player, tilesize: int = 16, rows: int = 32,
                 columns: int = 32, margin_height: int = 6,
                 margin_width: int = 6, initial_snake_size: int = 3,
                 bgcolor: pygame.Color = pygame.Color(0, 0, 0),
                 border_color: pygame.Color = pygame.Color(40, 40, 40),
                 food_color: pygame.Color = pygame.Color(200, 0, 0),
                 snake_color: pygame.Color = pygame.Color(255, 255, 255),
                 snake_head_color: pygame.Color = pygame.Color(255, 255, 255),
                 font_color: pygame.Color = pygame.Color(0, 200, 0),
                 tick_duration: float = 0.1,
                 title: str = "Snake Game", verbose: bool = False):
        """Create a PyGame UI object.

        Arguments:
        player (Player): The player object playing the game.


        Keyword arguments:
        tilesize (int): Size (height and width) of each 'tile' in the game.
        (default: 16)

        rows (int): Amount of rows on the game grid.
        (default: 32)

        columns (int): Amount of columns on the game grid.
        (default: 32)

        margin_height (int): Height of the margin around the game grid,
        must be at least 4. (default: 6)

        margin_width (int): Width of the margin around the game grid,
        must be at least 2. (default: 6)

        initial_snake_size (int): Initial size of the 'snake'.
        (default: 3)

        bgcolor (pygame.Color): Color of the game background.
        (default: pygame.Color(0, 0, 0))

        border_color (pygame.Color): Color of the border around the game grid.
        (default: pygame.Color(40, 40, 40))

        food_color (pygame.Color): Color of the 'food'.
        (default: pygame.Color(200, 0, 0))

        snake_color (pygame.Color): Color of the 'snake'.
        (default: pygame.Color(255, 255, 255))

        snake_head_color (pygame.Color): Color of the 'snake head'.
        (default: pygame.Color(255, 255, 255))

        font_color (pygame.Color): Color of the text.
        (default: pygame.Color(0, 200, 0))

        tick_duration (float): Minimum duration of each 'tick' (game loop).
        (default: 0.1)

        title (str): Title of the game window.
        (default: "Snake Game")

        verbose (bool): Flag that determines whether the output is verbose.
        (default: False)


        Raises:
        ValueError: For invalid values for margin height / width."""

        if margin_height < 4:
            raise ValueError("'margin_height' must be at least 4!")
        if margin_width < 2:
            raise ValueError("'margin_width' must be at least 2")

        self.player = player
        self.tilesize = tilesize
        self.rows = rows
        self.columns = columns
        self.margin_height = margin_height
        self.margin_width = margin_width
        self.initial_snake_size = initial_snake_size
        self.bgcolor = bgcolor
        self.border_color = border_color
        self.food_color = food_color
        self.snake_color = snake_color
        self.snake_head_color = snake_head_color
        self.font_color = font_color
        self.tick_duration = tick_duration
        self.title = title
        self.verbose = verbose
        self.game = Game(rows, columns, initial_snake_size)
        self.screen = pygame.display.set_mode(
            ((columns + margin_width * 2) * tilesize,
             (rows + margin_height * 2) * tilesize))
        self.font = None
        self.started = False
        self.input_thread = None

    def draw_game(self):
        """Draw game on screen.
        Raises RuntimeError if game hasn't started."""

        if not self.started:
            raise RuntimeError("Cannot call 'draw_game' before game starts")

        # Draw background
        self.screen.fill(self.bgcolor)

        # Draw borders
        pygame.draw.rect(self.screen, self.border_color,
                         pygame.Rect((self.margin_width - 1) * self.tilesize,
                                     (self.margin_height - 1) * self.tilesize,
                                     (self.columns + 2) * self.tilesize,
                                     self.tilesize))

        pygame.draw.rect(self.screen, self.border_color,
                         pygame.Rect((self.margin_width - 1) * self.tilesize,
                                     (self.margin_height - 1) * self.tilesize,
                                     self.tilesize,
                                     (self.rows + 2) * self.tilesize))

        pygame.draw.rect(self.screen, self.border_color,
                         pygame.Rect((self.margin_width - 1) * self.tilesize,
                                     (self.rows + self.margin_height) *
                                     self.tilesize, (self.columns + 2) *
                                     self.tilesize, self.tilesize))

        pygame.draw.rect(self.screen, self.border_color,
                         pygame.Rect((self.margin_width + self.columns) *
                                     self.tilesize,
                                     (self.margin_height - 1) * self.tilesize,
                                     self.tilesize,
                                     (self.rows + 2) * self.tilesize))

        # Draw Snake head
        pygame.draw.rect(self.screen, self.snake_head_color,
                         pygame.Rect((self.game.snake.head_pos.x +
                                      self.margin_width) * self.tilesize,
                                     (self.game.snake.head_pos.y +
                                      self.margin_height) * self.tilesize,
                                     self.tilesize, self.tilesize))

        # Draw Snake body
        for pos in self.game.snake.body_pos:
            pygame.draw.rect(self.screen, self.snake_color,
                             pygame.Rect((pos.x + self.margin_width) *
                                         self.tilesize,
                                         (pos.y + self.margin_height) *
                                         self.tilesize,
                                         self.tilesize, self.tilesize))

        # Draw Food
        pygame.draw.rect(self.screen, self.food_color,
                         pygame.Rect((self.game.food_pos.x +
                                      self.margin_width) * self.tilesize,
                                     (self.game.food_pos.y +
                                      self.margin_height) * self.tilesize,
                                     self.tilesize, self.tilesize))

        # Draw score text
        if self.game.is_game_over():
            text_surface = self.font.render('Game Over', True, self.font_color)

            self.screen.blit(text_surface, (((self.columns +
                                              self.margin_width * 2) *
                                             self.tilesize -
                                             text_surface.get_width()) / 2,
                                            ((self.rows +
                                              self.margin_height * 2) *
                                             self.tilesize -
                                             text_surface.get_height()) / 2))
        else:
            if self.game.paused:
                text_surface = self.font.render('PAUSED', True, self.font_color)

            else:
                text_surface = self.font.render(f'Score: {self.game.snake.score}',
                                                True, self.font_color)

            self.screen.blit(text_surface, (((self.columns +
                                              self.margin_width * 2) *
                                             self.tilesize -
                                             text_surface.get_width()) / 2,
                                            ((self.margin_height - 1) *
                                             self.tilesize -
                                             text_surface.get_height()) / 2))

        pygame.display.update()

    def update_game(self):
        """Update the game
        Raises RuntimeError if game hasn't started"""

        if not self.started:
            raise RuntimeError("Cannot call 'update_game' before game starts")

        # Close the game if a Quit event is found
        if pygame.event.peek(pygame.QUIT):
            exit(0)

        # Check for game paused
        if not self.game.paused:
            if self.verbose:
                print(f"Head: {self.game.snake.head_pos}")
                print("\n".join([f"Body {i+1}: {body}" for (i, body) in
                                enumerate(self.game.snake.body_pos)]))
                print(f"Food: {self.game.food_pos}")
                print(f"Self-collision: {self.game.snake.self_collision()}")

            # Wait for player input if necessary
            if self.player.wait_for_input:
                self.player.get_input()

            self.game.update()

    def start_game(self):
        """Start the game.
        Raises RuntimeError if game has already started."""

        if self.started:
            raise RuntimeError("Game has already started!")

        pygame.init()
        pygame.display.set_caption(self.title)

        self.font = pygame.font.SysFont(pygame.font.get_default_font(),
                                        self.tilesize * 2)
        self.player.start(self.game)

        if not self.player.wait_for_input:
            self.input_thread = threading.Thread(target=self.player.input_loop,
                                                 daemon=True)
            self.input_thread.start()

        self.started = True
        self.draw_game()

        while not self.game.is_game_over():
            start = time.perf_counter()
            self.update_game()
            self.draw_game()
            end = time.perf_counter()
            elapsed_time = float(end - start)

            print("Tick took " + str(elapsed_time) + " seconds")

            if elapsed_time < self.tick_duration:
                time.sleep(self.tick_duration - elapsed_time)

        self.end_game()

    def end_game(self):
        """End the game
        Raises RuntimeError if game hasn't started"""

        if not self.started:
            raise RuntimeError("Cannot call 'update_game' before game starts")

        if self.input_thread:
            self.input_thread.join()

        if self.verbose:
            print("Game Over!")

        self.started = False

        pygame.event.clear()
        pygame.event.wait()
