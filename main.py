from ui import PyGameHuman, PyGameUI
from noui import NoUI
from ai import AI

# TODO: Use abstract classes for Game and UI

use_ai = False
use_ui = True
ui_verbose = False

if use_ai:
    player = AI()
else:
    player = PyGameHuman()

if use_ui:
    snake_game = PyGameUI(player, verbose=ui_verbose)
else:
    snake_game = NoUI(player, verbose=ui_verbose)

snake_game.start_game()
