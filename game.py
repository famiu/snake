import typing
import random
import utils
from snake import Snake


class Game:
    """Snake Game"""

    def __init__(self, rows=32, columns=32, initial_snake_size=3):
        if rows < initial_snake_size * 3 or columns < initial_snake_size * 3:
            raise ValueError("Game row and column size must be at least "
                             "thrice the value of Snake's intiial size")

        self.rows = rows
        self.cols = columns
        self.snake = Snake(head_pos=utils.Pos(round(rows/2),
                                              round(columns/2)))
        self.food_pos = utils.Pos(random.randint(0, columns-1),
                                  random.randint(0, rows-1))
        self.paused = False

        # List of all positions, useful when spawning food
        self.all_positions = set([
            utils.Pos(i, j) for j in range(self.rows) for i in range(self.cols)
        ])

    def get_snake_positions(self) -> typing.List[utils.Pos]:
        """Get a list of all positions with a part of the snake (including head)"""

        return [self.snake.head_pos] + self.snake.body_pos

    def is_grid_empty(self, grid_pos: utils.Pos):
        """Check if a certain grid in the map is empty"""

        if self.snake.head_pos == grid_pos:
            return False

        for snake_part in self.snake.body_pos:
            if snake_part == grid_pos:
                return False

        return True

    def create_new_food(self):
        """Create new food object on the grid"""

        # First, create a list of empty grids
        # empty_positions: typing.List[utils.Pos] = []
        # for i in range(self.cols):
        #     for j in range(self.rows):
        #         if self.is_grid_empty(utils.Pos(i, j)):
        #             empty_positions.append(utils.Pos(i, j))

        # Then choose a random grid from the list to generate the food
        # self.food_pos = random.choice(empty_positions)

        empty_positions = self.all_positions - set(self.get_snake_positions())

        self.food_pos = random.choice(tuple(empty_positions))

    def food_collision(self) -> bool:
        """Check if Snake is colliding with food"""

        # Since only the snake's head can take the food
        # only check if the head is colliding with the food
        return self.food_pos == self.snake.head_pos

    def is_game_over(self) -> bool:
        """Check if game is over"""

        # If Snake's body parts collide with each other, return True
        if self.snake.self_collision():
            return True

        # Otherwise, since only head and tail update each time the snake moves,
        # only check if the head or the tail cross the borders
        return (self.snake.head_pos.x not in range(self.cols) or
                self.snake.head_pos.y not in range(self.rows) or
                self.snake.body_pos[-1].x not in range(self.cols) or
                self.snake.body_pos[-1].y not in range(self.rows))

    def set_snake_direction(self, direction: utils.Direction):
        """Function to set the snake direction to control the game through a frontend"""

        # Check if new direction is opposite of current direction
        # by adding the Vector forms of the direction and checking
        # if it is (0, 0), since the sum of two opposing vectors is 0.
        # If not, set the direction
        # Since 2 successive keypresses can still cause an issue
        # if snake.direction is used, just get the offset of
        # snake.head and first body part instead
        if(self.snake.head_pos - self.snake.body_pos[0] +
           utils.Pos.from_dir(direction)) != utils.Pos(0, 0):
            self.snake.direction = direction

    def update(self):
        """Update the whole game"""

        # If game is paused, do nothing
        if self.paused:
            return

        # Firstly, move the snake
        self.snake.move()

        # Then, if snake collided with food, increase score,
        # grow the snake and create new food.
        if self.food_collision():
            self.snake.score += 1
            self.snake.grow()
            self.create_new_food()
