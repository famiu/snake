import time
import threading
from game import Game
from player import Player


class NoUI:
    """No-UI frontend for Snake Game"""

    def __init__(self, player: Player, rows: int = 32, columns: int = 32,
                 initial_snake_size: int = 3, tick_duration: float = 0.1,
                 verbose: bool = False):
        """Create a PyGame UI object.

        Arguments:
        player (Player): The player object playing the game.


        Keyword arguments:
        rows (int): Amount of rows on the game grid.
        (default: 32)

        columns (int): Amount of columns on the game grid.
        (default: 32)

        initial_snake_size (int): Initial size of the 'snake'.
        (default: 3)

        tick_duration (float): Minimum duration of each 'tick' (game loop).
        (default: 0.1)

        verbose (bool): Flag that determines whether the output is verbose.
        (default: False)"""

        self.game = Game(rows, columns, initial_snake_size)
        self.player = player
        self.tick_duration = tick_duration
        self.verbose = verbose
        self.input_thread = None
        self.started = False

    def draw_game(self):
        """Draw game"""
        pass

    def update_game(self):
        """Update the game
        Raises RuntimeError if game hasn't started"""

        if not self.started:
            raise RuntimeError("Cannot call 'update_game' before game starts")

        # Wait for player input if necessary
        if not self.game.paused:
            if self.verbose:
                print(f"Head: {self.game.snake.head_pos}")
                print("\n".join([f"Body {i+1}: {body}" for (i, body) in
                                enumerate(self.game.snake.body_pos)]))
                print(f"Food: {self.game.food_pos}")
                print(f"Self-collision: {self.game.snake.self_collision()}")

            # Wait for player input if necessary
            if self.player.wait_for_input:
                self.player.get_input()

            self.game.update()

    def start_game(self):
        """Start the game.
        Raises RuntimeError if game has already started."""

        if self.started:
            raise RuntimeError("Game has already started!")

        self.player.start(self.game)

        if not self.player.wait_for_input:
            self.input_thread = threading.Thread(target=self.player.input_loop,
                                                 daemon=True)
            self.input_thread.start()

        self.started = True
        self.draw_game()

        while not self.game.is_game_over():
            start = time.perf_counter()
            self.update_game()
            self.draw_game()
            end = time.perf_counter()
            elapsed_time = float(end - start)

            print("Tick took " + str(elapsed_time) + " seconds")

            if elapsed_time < self.tick_duration:
                time.sleep(self.tick_duration - elapsed_time)

        self.end_game()

    def end_game(self):
        """End the game
        Raises RuntimeError if game hasn't started"""
        if not self.started:
            raise RuntimeError("Cannot call 'update_game' before game starts")

        if self.input_thread:
            self.input_thread.join()

        if self.verbose:
            print("Game Over!")

        self.started = False
