from game import Game

"""Module containing reference implementation of Player for Snake Game"""


class Player:
    """Abstract class for Player entity that controls the Snake."""

    def __init__(self, wait_for_input: bool):
        """Initialize Player object.

        Arguments:
        wait_for_input (bool): Determines if the UI should wait for input.
        If True, get_input() will be called every update.
        If False, input_loop() will be asynchronously run
                  after game starts instead"""

        self.wait_for_input = wait_for_input
        self.game = None

    def start(self, game: Game):
        """Start the Player object, by loading the game,
        and analyzing it if necessary.

        Arguments:
        game (Game): The game being played"""

        self.game = game

    @property
    def game_ended(self):
        return self.game and self.game.is_game_over()
